package com.open1111.service;

import com.open1111.entity.Manager;

/**
 * @author liugang
 * @create 2018/3/8 23:06
 **/
public interface ManagerService {

    /**
     * 通过用户名查找实体
     * @param userName
     * @return
     */
    public Manager getManagerByUserName(String userName);


}
