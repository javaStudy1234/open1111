package com.open1111.service.impl;

import com.open1111.dao.ArticleDao;
import com.open1111.entity.Article;
import com.open1111.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 帖子service实现类
 * @author liugang
 * @create 2018/3/15 22:47
 **/
@Service("articleService")
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleDao arcticleDao;

    public List<Article> getNewest() {
        return arcticleDao.getNewest();
    }

    public List<Article> getRecommend() {
        return arcticleDao.getRecommend();
    }

    public List<Article> getSlide() {
        return arcticleDao.getSlide();
    }

    public List<Article> getIndex(Integer typeId) {
        return arcticleDao.getIndex(typeId);
    }

    public Article findById(Integer id) {
        return arcticleDao.findById(id);
    }

    public Article getLastArticle(Integer id) {
        return arcticleDao.getLastArticle(id);
    }

    public Article getNextArticle(Integer id) {
        return arcticleDao.getNextArticle(id);
    }

    public Integer update(Article article) {
        return arcticleDao.update(article);
    }

    public List<Article> list(Map<String, Object> map) {
        return arcticleDao.list(map);
    }

    public Long getTotal(Map<String, Object> map) {
        return arcticleDao.getTotal(map);
    }
}
