package com.open1111.service.impl;

import com.open1111.dao.ArcTypeDao;
import com.open1111.entity.ArcType;
import com.open1111.service.ArcTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * arcTypeService实现类
 * @author liugang
 * @create 2018/3/14 22:10
 **/
@Service("arcTypeService")
public class ArcTypeServiceImpl implements ArcTypeService{

    @Autowired
    private ArcTypeDao arcTypeDao;

    public List<ArcType> arcTypeList(Map<String, Object> map) {
        return arcTypeDao.arcTypeList(map);
    }

    public ArcType findById(Integer id) {
        return arcTypeDao.findById(id);
    }
}
