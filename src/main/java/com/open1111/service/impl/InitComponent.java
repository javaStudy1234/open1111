package com.open1111.service.impl;

import com.open1111.entity.ArcType;
import com.open1111.entity.Article;
import com.open1111.entity.Link;
import com.open1111.service.ArcTypeService;
import com.open1111.service.ArticleService;
import com.open1111.service.LinkService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.ArrayList;
import java.util.List;

/**
 * 初始化组件
 * @author liugang
 * @create 2018/3/11 22:37
 **/
@Component("initComponent")
public class InitComponent implements ApplicationContextAware,ServletContextListener{

    private static ApplicationContext applicationContext;

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * 刷新application缓存的方法
     * @param application
     */
    public void refreshSystem(ServletContext application){
        //强转成LinkService，得到这个bean
        LinkService linkService = (LinkService) applicationContext.getBean("linkService");
        List<Link> linkList = linkService.linkList(null);
        //再将linkList放到application缓存里面去
        application.setAttribute("linkList",linkList);

        ArcTypeService arcTypeService = (ArcTypeService) applicationContext.getBean("arcTypeService");
        List<ArcType> arcTypeList = arcTypeService.arcTypeList(null);
        application.setAttribute("arcTypeList",arcTypeList);

        ArticleService articleService = (ArticleService) applicationContext.getBean("articleService");
        List<Article> newestArticleList = articleService.getNewest();//获取最新7条帖子
        application.setAttribute("newestArticleList",newestArticleList);

        List<Article> recommendArticleList = articleService.getRecommend();
        application.setAttribute("recommendArticleList",recommendArticleList);

        List<Article> slideArticleList = articleService.getSlide();
        application.setAttribute("slideArticleList",slideArticleList);

        List allIndexArticleList = new ArrayList();//存储多个集合，每个集合是指定类型的帖子的最新8条数据
        //判断，如果类别不为空的话
        if(arcTypeList!=null && arcTypeList.size()!=0){
            for(int i=0;i<arcTypeList.size();i++){
                List<Article> subArticleList = articleService.getIndex(arcTypeList.get(i).getId());
                allIndexArticleList.add(subArticleList);
            }
        }
        application.setAttribute("allIndexArticleList",allIndexArticleList);
    }

    public void contextInitialized(ServletContextEvent sce) {
        ServletContext application = sce.getServletContext();
        refreshSystem(application);
    }

    public void contextDestroyed(ServletContextEvent sce) {

    }
}
