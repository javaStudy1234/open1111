package com.open1111.service.impl;

import com.open1111.dao.CalendarDao;
import com.open1111.entity.Calendar;
import com.open1111.service.CalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liugang
 * @create 2018/3/13 22:29
 **/
@Service("/calendarService")
public class CalendarServiceImpl implements CalendarService{

    @Autowired
    private CalendarDao calendarDao;

    public List<Calendar> calendarList(){
        return calendarDao.calendarList();
    }
}
