package com.open1111.service.impl;

import com.open1111.dao.ManagerDao;
import com.open1111.entity.Manager;
import com.open1111.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 管理员Service实现类
 * @author liugang
 * @create 2018/3/8 23:06
 **/
@Service("managerService")
public class ManagerServiceImpl implements ManagerService{

    @Autowired
    private ManagerDao managerDao;

    public Manager getManagerByUserName(String userName) {
        return managerDao.getManagerByUserName(userName);
    }
}
