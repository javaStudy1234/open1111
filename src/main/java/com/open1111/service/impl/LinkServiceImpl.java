package com.open1111.service.impl;

import com.open1111.dao.LinkDao;
import com.open1111.entity.Link;
import com.open1111.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * linkService的实现类
 * @author liugang
 * @create 2018/3/14 21:13
 **/
@Service("linkService")
public class LinkServiceImpl implements LinkService{

    @Autowired
    private LinkDao linkDao;

    public List<Link> linkList(Map<String, Object> map) {
        return linkDao.linkList(map);
    }
}
