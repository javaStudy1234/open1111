package com.open1111.service;


import com.open1111.entity.Link;

import java.util.List;
import java.util.Map;

/**
 * @author liugang
 * @create 2018/3/8 23:06
 **/
public interface LinkService {

    List<Link> linkList(Map<String,Object> map);


}
