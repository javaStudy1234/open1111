package com.open1111.service;

import com.open1111.entity.Calendar;

import java.util.List;

/**
 * @author liugang
 * @create 2018/3/13 22:28
 **/
public interface CalendarService {

    public List<Calendar> calendarList();
}
