package com.open1111.dao;

import com.open1111.entity.ArcType;

import java.util.List;
import java.util.Map;

/**
 * 帖子类别Dao接口
 * @author liugang
 * @create 2018/3/14 20:55
 **/
public interface ArcTypeDao {

    /**
     * 分页查询
     * @param map
     * @return
     */
    List<ArcType> arcTypeList(Map<String,Object> map);

    /**
     * 根据id查询实体
     * @param id
     * @return
     */
    ArcType findById(Integer id);
}
