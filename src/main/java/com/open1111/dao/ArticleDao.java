package com.open1111.dao;

import com.open1111.entity.Article;

import java.util.List;
import java.util.Map;

/**
 * @author liugang
 * @create 2018/3/15 20:06
 **/
public interface ArticleDao {

    /**
     * 获取最新的7条帖子
     * @return
     */
    List<Article> getNewest();

    /**
     * 获取最新7条推荐的帖子啊
     * @return
     */
    List<Article> getRecommend();

    /**
     * 获取最新5条幻灯的数据
     * @return
     */
    List<Article> getSlide();

    /**
     * 根据帖子类别来查找最新八条数据
     * @param typeId
     * @return
     */
    List<Article> getIndex(Integer typeId);

    /**
     * 通过id查询帖子
     * @param id
     * @return
     */
    Article findById(Integer id);

    /**
     * 获取上一个article
     * @param id
     * @return
     */
    Article getLastArticle(Integer id);

    /**
     * 获取下一个article
     * @param id
     * @return
     */
    Article getNextArticle(Integer id);

    /**
     * 更新帖子的点击次数
     * @param article
     * @return
     */
    Integer update(Article article);

    /**
     * 根据条件分页查询帖子
     * @param map
     * @return
     */
    List<Article> list(Map<String,Object> map);

    /**
     * 获取总记录数
     * @param map
     * @return
     */
    Long getTotal(Map<String,Object> map);
}
