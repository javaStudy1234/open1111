package com.open1111.dao;

import com.open1111.entity.Manager;

/**
 * 管理员Dao接口
 * @author liugang
 * @create 2018/3/8 22:49
 **/
public interface ManagerDao {

    /**
     * 通过用户名查找实体
     * @param userName
     * @return
     */
    public Manager getManagerByUserName(String userName);

}
