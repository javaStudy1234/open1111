package com.open1111.dao;

import com.open1111.entity.Link;

import java.util.List;
import java.util.Map;

/**
 * 友情链接Dao接口
 * @author liugang
 * @create 2018/3/14 20:55
 **/
public interface LinkDao {

    List<Link> linkList(Map<String,Object> map);
}
