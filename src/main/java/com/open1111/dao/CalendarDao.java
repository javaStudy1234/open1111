package com.open1111.dao;

import com.open1111.entity.Calendar;
import com.open1111.entity.Manager;

import java.util.List;

/**
 * 管理员Dao接口
 * @author liugang
 * @create 2018/3/13 22:32
 **/
public interface CalendarDao {

    /**
     * 得到所有的calendar的list集合
     * @return
     */
    public List<Calendar> calendarList();

}
