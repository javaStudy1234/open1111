package com.open1111.util;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * Md5加密工具
 * @author liugang
 * @create 2018/3/8 23:50
 **/
public class Md5Util {
    //定义为静态常量，因为登录的时候也要用到
    public static final String SATL = "open1111";

    public static String md5(String str,String salt){
        return new Md5Hash(str,salt).toString();
    }

    public static void main(String[] args) {
        String password = "123456";
        System.out.println("加密后的密码为："+md5(password,Md5Util.SATL));
    }
}
