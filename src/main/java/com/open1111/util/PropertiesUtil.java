package com.open1111.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * properties配置文件工具类
 * @author liugang
 * @create 2018/3/18 0:27
 **/
public class PropertiesUtil {

    /**
     * 根据key获取value
     * @param key
     * @return
     */
    public static String getValue(String key){
        Properties prop = new Properties();
        PropertiesUtil pu = new PropertiesUtil();
        InputStream in = pu.getClass().getResourceAsStream("/open1111.properties");
        try{
            prop.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop.getProperty(key);
    }
}
