package com.open1111.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 字符串工具类
 * @author liugang
 * @create 2018/3/18 11:16
 **/
public class StringUtil {
    /**
     * 判断是否是空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){
        if(str==null || "".equals(str)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 判断是否不为空
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str){
        if(str!=null && "".equals(str)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 过滤掉集合中的空格元素
     * @param list
     * @return
     */
    public static List<String> filterWhite(List<String> list){
        List<String> resultList = new ArrayList<String>();
        for(String l:list){
            if(isNotEmpty(l)){
                resultList.add(l);
            }
        }
        return resultList;
    }
}
