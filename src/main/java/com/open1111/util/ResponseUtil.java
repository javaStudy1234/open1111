package com.open1111.util;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * ajax返回输出流工具类
 * @author liugang
 * @create 2018/3/9 19:14
 **/
public class ResponseUtil {

    /**
     * 页面刷新
     * @param response
     * @param o
     * @throws Exception
     */
    public static void write(HttpServletResponse response,Object o)throws Exception{
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.println(o.toString());
        out.flush();
        out.close();
    }
}
