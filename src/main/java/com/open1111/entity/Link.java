package com.open1111.entity;

/**
 * @author liugang
 * @create 2018/3/14 20:55
 **/
public class Link {

    private Integer id;//编号
    private String name;//名称
    private String url;//链接
    private Integer sortNo;//排序

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }
}
