package com.open1111.controller;

import com.open1111.entity.Manager;
import com.open1111.service.ManagerService;
import com.open1111.util.Md5Util;
import com.open1111.util.ResponseUtil;
import net.sf.json.JSONObject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * 管理员Controller层
 * @author liugang
 * @create 2018/3/8 23:10
 **/
@Controller
@RequestMapping("/login")
public class ManagerController {

    @Resource
    private ManagerService managerService;

    /**
     * 用户登录
     * @param manager
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping("/check")
    public String login(Manager manager, HttpServletResponse response) throws Exception {
        Subject subject = SecurityUtils.getSubject();
        //右边是把页面传过来的明文密码进行加密处理
        UsernamePasswordToken token = new UsernamePasswordToken(manager.getUserName(), Md5Util.md5(manager.getPassword(),Md5Util.SATL));
        JSONObject result = new JSONObject();
        try{
            subject.login(token);
            result.put("success", true);
        }catch (Exception e){
            result.put("success",false);
            result.put("errorInfo","用户名或密码信息错误!");
            e.printStackTrace();
        }
        ResponseUtil.write(response,result);
        return null;
    }

    /**
     * 测试接口
     * @return
     */
    @RequestMapping("/test")
    public String test(){
        return "index";
    }
}
