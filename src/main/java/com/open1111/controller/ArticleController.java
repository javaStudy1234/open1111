package com.open1111.controller;

import com.open1111.entity.ArcType;
import com.open1111.entity.Article;
import com.open1111.service.ArticleService;
import com.open1111.util.NavUtil;
import com.open1111.util.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * 帖子controller层
 * @author liugang
 * @create 2018/3/17 16:49
 **/
@Controller
@RequestMapping("/article")
public class ArticleController {

    @Resource
    private ArticleService articleService;

    @RequestMapping("/{id}")
    public ModelAndView details(@PathVariable("id") Integer id, HttpServletRequest request)throws Exception{
        ModelAndView mav = new ModelAndView();
        Article article = articleService.findById(id);
        String keyWords = article.getKeyWords();
        if(StringUtil.isNotEmpty(keyWords)){
            String arr[] = keyWords.split(" ");
            //Arrays.asList的使用
            mav.addObject("keyWords",StringUtil.filterWhite(Arrays.asList(arr)));
        }else{
            mav.addObject("keyWords",null);
        }
        //request.getServletContext().getContextPath()获取项目绝对路径
        mav.addObject("pageCode",this.getUpAndDownPageCode(articleService.getLastArticle(id),articleService.getNextArticle(id),request.getServletContext().getContextPath()));
        mav.addObject("article",article);
        article.setClick(article.getClick()+1);
        articleService.update(article);
        ArcType arcType = article.getArcType();
        mav.addObject("navCode", NavUtil.genArticleNavigation(arcType.getTypeName(),arcType.getId(),article.getTitle()));
        mav.setViewName("article");
        return mav;
    }

    /**
     * 获取下一篇帖子和上一篇帖子代码
     * @param lastArticle
     * @param nextArticle
     * @param projectContext
     * @return
     */
    @RequestMapping("/")
    public String getUpAndDownPageCode(Article lastArticle, Article nextArticle, String projectContext){
         StringBuffer pageCode = new StringBuffer();
         if(lastArticle==null || lastArticle.getId() == null){
             pageCode.append("<p>上一篇：没有了</p>");
         }else{
             pageCode.append("<p>上一篇：<a href='/article/"+lastArticle.getId()+".html'>"+lastArticle.getTitle()+"</a></p>");
         }

         if(nextArticle==null || nextArticle.getId() == null){
             pageCode.append("<p>下一篇：没有了</p>");
         }else{
             pageCode.append("<p>下一篇：<a href='/article/"+nextArticle.getId()+".html'>"+nextArticle.getTitle()+"</a></p>");
         }
         return pageCode.toString();
    }

}
