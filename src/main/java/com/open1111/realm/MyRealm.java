package com.open1111.realm;

import com.open1111.entity.Manager;
import com.open1111.service.ManagerService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;

/**
 * @author liugang
 * @create 2018/3/8 23:12
 **/
public class MyRealm extends AuthorizingRealm{

    @Resource
    private ManagerService managerService;

    /**
     * 为当前登录用户授权
     * @param principalCollection
     * @return
     */
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    /**
     * 验证当前登录的用户
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String userName = authenticationToken.getPrincipal().toString();
        Manager manager = managerService.getManagerByUserName(userName);
        if(manager!=null){
            //设置session
            SecurityUtils.getSubject().getSession().setAttribute("currentUser",manager);
            //第三个参数随便取
            AuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(manager.getUserName(),manager.getPassword(),"11");
            return authenticationInfo;
        }else {
            return null;
        }
    }
}
