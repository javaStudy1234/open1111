<%--
  Created by IntelliJ IDEA.
  User: 33822
  Date: 2018/3/13
  Time: 22:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/fullcalendar/fullcalendar.print.min.css">\
    <script src="${pageContext.request.contextPath}/static/fullcalendar/fullcalendar.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/fullcalendar/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/fullcalendar/moment.min.js"></script>
    <style>
        body {
            margin: 40px 10px;
            padding: 0;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div id="calendar"></div>
</body>
</html>
