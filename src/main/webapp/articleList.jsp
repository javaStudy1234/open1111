<%--
  Created by IntelliJ IDEA.
  User: 33822
  Date: 2018/3/8
  Time: 23:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
    <title>${arcType.typeName}</title>
    <meta name="keywords" content="${arcType.keywords}"/>
    <meta name="description" content="${arcType.description}"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/open1111.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jQuery.js"></script>
</head>
<body>
<jsp:include page="/common/head.jsp"></jsp:include>
<jsp:include page="/common/menu.jsp"></jsp:include>

<div class="content">

    <div class="w960">

        <div class="pLeft">
            <div class="data_list">
                <div class="dataHeader navi">${navCode}</div>
                <div class="datas article_type_list">
                    <ul>
                        <c:forEach var="article" items="${articleList}">
                            <li><a target="_blank" href="${pageContext.request.contextPath}/arcType/${article.id}.html" title="${article.title}">
                                [<fmt:formatDate value="${article.publishDate}" pattern="MM-dd"/>]&nbsp;&nbsp;<font color="${article.titleColor}">
                                ${article.title}
                            </font>
                            </a></li>
                        </c:forEach>
                    </ul>
                </div>

                <div class="upAndDownPage">
                    ${pageCode}
                </div>
            </div>
        </div>

        <div class="pRight">

            <div class="data_list" >
                <div class="dataHeader">站长推荐</div>
                <div class="datas">
                    <ul>
                        <c:forEach var="article" items="${recommendArticleList}">

                            <li><a target="_blank" href="${pageContext.request.contextPath}/article/${article.id}.html"
                                   title="${article.title}"><font color="${article.titleColor}">${fn:substring(article.title, 0,16 )}</font></a></li>

                        </c:forEach>
                    </ul>
                </div>
            </div>

            <div class="data_list" style="margin-top: 10px">
                <div class="dataHeader">最近更新</div>
                <div class="datas">
                    <ul>
                        <c:forEach items="${newestArticleList}" var="article">

                            <li><a target="_blank" href="${pageContext.request.contextPath}/article/${article.id}.html"
                                   title="${article.title}"><font
                                    color="${article.titleColor}">${fn:substring(article.title,0 ,16 )}</font></a></li>

                        </c:forEach>

                    </ul>
                </div>
            </div>

        </div>

    </div>

</div>


<jsp:include page="/common/foot.jsp"></jsp:include>
</body>
</html>
